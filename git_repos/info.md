# TOP Git Repos

- [awesome-hacking](https://github.com/carpedm20/awesome-hacking)
- [Gobuster](https://github.com/OJ/gobuster)
- [awesome-ml-for-cybersecurity](https://github.com/jivoi/awesome-ml-for-cybersecurity)
- [Breached Passwords](https://github.com/hmaverickadams/breach-parse)
- [SecLists](https://github.com/danielmiessler/SecLists)
- [Bloodhound](https://github.com/BloodHoundAD/BloodHound)
- https://github.com/StevenBlack/hosts
- Ohmyzsh: https://github.com/robbyrussell/oh-my-zsh
- Oh My Fish!: https://github.com/oh-my-fish